﻿using Microsoft.Extensions.Logging;
using Microsoft.Extensions.PlatformAbstractions;
using Mono.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace jsoncat
{
    public class Program
    {
        private const string USAGE_INFO = @"NAME
       javacat - concatenate json files and print on the standard output

SYNOPSIS
       javacat [OPTION]... [FILE]...

DESCRIPTION
       Concatenate FILE(s), or standard input, to standard output.

       -C, --compact
              remove newlines and redundant spaces.

       -P, --pretty
              pretty print the output.

       -I=n, --indent=n
              sets the indent level to n. works only if --pretty is set.

       -H, --help
              display this help and exit.

       -V, --version
              output version information and exit.

EXAMPLES
       jsoncat f - g
              Output f's contents, then standard input, then g's contents.

       jsoncat
              Copy standard input to standard output.

AUTHOR
       Written by Harry Y.

REPORTING BUGS
       Report jsoncat bugs: <https://github.com/holojson/tools/issues>

COPYRIGHT
       Copyright (c) 2016 Harry Y - MIT License <https://github.com/holojson/tools/blob/master/LICENSE>
       This is free software: you are free to change and redistribute it. 
       There is NO WARRANTY, to the  extent  permitted by law.

SEE ALSO
       jsonjoin(1)
";

        private static ILogger logger;

        // global error message (e.g., due to invalid command line args, etc.)
        private static string errorMessage = null;
        // warning message is displayed, but the program does not stop. 
        private static string warningMessage = null;

        // For all apps.
        // If it is set, all other args are ignored.
        private static bool isArgHelp = false;
        // Likewise...
        private static bool isArgVersion = false;
        // Both isCompact and isPretty cannot be true.
        private static bool isCompact = false;
        private static bool isPretty = false;
        private static int indentLevel = 0;

        public static void Main(string[] args)
        {
            Console.WriteLine("Main() called.");

            // Cf. https://msdn.microsoft.com/en-us/magazine/mt694089.aspx
            ILoggerFactory loggerFactory = new LoggerFactory().AddConsole().AddDebug();
            logger = loggerFactory.CreateLogger<Program>();

            IList<string> mainArgs = null;
            try {
                // cf. https://github.com/mono/mono/blob/master/mcs/class/Mono.Options/Mono.Options/Options.cs
                var opts = new OptionSet() {
                    { "H|help", v => isArgHelp = true },
                    { "V|version", v => isArgVersion = true },
                    { "C|compact", v => isCompact = true },
                    { "P|pretty", v => isPretty = true },
                    { "I=|indent=", (int i) => indentLevel = i },
                };
                mainArgs = opts.Parse(args);
                Console.WriteLine($"mainArgs = {string.Join(",", mainArgs)}");

                // Validate args.
                if (isCompact == true && isPretty == true) {
                    // error
                    errorMessage = "-compact and -pretty cannot both be specified.";
                    // isArgHelp = true;   // Just show usage.
                } else if(isCompact == true && indentLevel > 0) {
                    warningMessage = "-indent will be ignored when -compact is set.";
                }
            } catch(Exception ex) {
                logger.LogWarning($"Failed to parse command line arguments: {ex.Message}");
                // Just display usage ??? 
                // isArgHelp = true;   // Just show usage.
                errorMessage = $"Invalid command line arguments. {ex.Message}";
            }
            DoRealMain(mainArgs);
        }


        private static void PrintErrorMessage()
        {
            logger.LogDebug("PrintErrorMessage() called.");

            if (errorMessage != null) {
                Console.WriteLine($"ERROR: {errorMessage}");
                Console.WriteLine(" ");
            }
        }
        private static void PrintWarningMessage()
        {
            logger.LogDebug("PrintWarningMessage() called.");

            if (warningMessage != null) {
                Console.WriteLine($"WARNING: {warningMessage}");
                Console.WriteLine(" ");
            }
        }
        private static void PrintUsageHelp()
        {
            logger.LogDebug("PrintUsageHelp() called.");

            Console.WriteLine(USAGE_INFO);
        }
        private static void PrintVersionInfo()
        {
            logger.LogDebug("PrintVersionInfo() called.");

            var version = PlatformServices.Default.Application.ApplicationVersion;
            logger.LogInformation($">>>> Version = {version}.");
            Console.WriteLine(version);
        }

        // mainArgs cannot be null.
        private static void DoRealMain(IList<string> mainArgs)
        {
            logger.LogDebug("DoRealMain() called.");

            if(! string.IsNullOrEmpty(errorMessage)) {
                PrintErrorMessage();
            } else {
                if (isArgHelp) {
                    PrintUsageHelp();
                } else if (isArgVersion) {
                    PrintVersionInfo();
                } else {
                    if (!string.IsNullOrEmpty(warningMessage)) {
                        PrintWarningMessage();
                    }

                    // ...

                    if (isCompact) {


                    } else if (isPretty) {
                        logger.LogInformation($">>>> indentLevel = {indentLevel}.");


                    } else {


                    }

                }
            }
        }

    }
}
